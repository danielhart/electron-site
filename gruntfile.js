module.exports = function(grunt) { // the general grunt function that is run

    grunt.initConfig({ // here we setup our config object with package.json and all the tasks

        pkg: grunt.file.readJSON('package.json'),

        sass: { // sass tasks
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'css/style.css': 'src/scss/style.scss' // this is our main scss file
                }
            }
        },

        cssmin: { // minifying css task
            dist: {
                files: {
                    'css/style.min.css': 'css/style.css'
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    'js/script.min.js': ['node_modules/konami-code/KonamiCode.js',
                                         'src/js/script.js']
                }
            }
        },

        watch: { // watch task for general work
            sass: {
                files: ['scss/**/*.scss'],
                tasks: ['sass']
            },
            styles: {
                files: ['css/style.css'],
                tasks: ['cssmin']
            }
        }
    });

    // all the plugins that is needed for above tasks
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');

    // registering the default task that we're going to use along with watch
    grunt.registerTask('styles', ['sass', 'cssmin']);
    grunt.registerTask('scripts', ['uglify']);
    grunt.registerTask('deploy', ['styles', 'scripts']);
    grunt.registerTask('default', 'deploy');
};